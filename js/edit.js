/**
 * Edit properties of image
 */

$j(document).ready(function(){
	//Edit button
	$j('.smpl_edit_plus').mouseover(function(){
		$j(this).css('cursor','pointer');
	});

	$j('.smpl_edit_plus').mouseout(function(){
		$j(this).css('cursor','default');
	});
	
 	/**
 	 * Edit command to show / hide the Edit form of properties
 	 * with parameters of actual item
 	 */
	var SmplEditForm   = $j("div#SmplEditForm");

	// Draggable window
	$j(function(){
		SmplEditForm.draggable();	
	})
	
	$j(".smpl_edit_plus").click(function(e){
		var id  = $j(this).attr('id').substring(6);
		var visible = SmplEditForm.css('display');
	
		pos = $j(this).position();
		if(visible =='none'){	// Load form from server with ajax
			var url  = smpl.ajax + "/edit/" + id + "/form";
			$j.ajax({
				url: url,
				type: "GET",
				success: function(data){
					$j("input#SmplEditFormSubmit").before(data);
					setDivInWindow(SmplEditForm, pos , e.pageX, e.pageY);
				},
				error :  function(data){
					alert(data);
				},
			});
		}else{
			/**
			 * Are you sure to cancel all modify
			 */
			var id = $j('input#smpl_id').val();
			if(id.length >0 && window.confirm("Are you sure to cancel the modifies?")){
				$j("div#smpl_new_tax").unbind('click');
				SmplEditForm.hide();
				$j("div#SmplFormTable").remove();
			}
		}
	});

	/**
	 * click on cancel button of windows of properties
 	*/
	$j(".smpl_edit_form_cancel").click(function(){
		var i = $j('input[type="hidden"]').filter(function(index){
			val = $j(this).val();
			ok = (val == "n"|| val=="m" || val =="d");
			return ok;
		});

		if(i.length >0){
			if(window.confirm("Are you sure to cancel the modifies?")){
				$j("div#smpl_new_tax").unbind('click');
				$j("div#SmplEditForm").hide();
				$j("div#SmplFormTable").remove();
			}
		}else{
			$j("div#smpl_new_tax").unbind('click');
			$j("div#SmplEditForm").hide();
			$j("div#SmplFormTable").remove();
		}
	});
	
	/**
	 * Edit form send to server
	 */
	$j("input#SmplEditFormSubmit").click(function(){		
		//Save the datas of form to server
		var formData = "";		
		$j('form#SmplEditForm input, form#SmplEditForm textarea').each( function(){
			var input = $j(this);
			formData += input.attr("name") + "=" + input.attr("value")+"&";
		});

		var id  = $j("input#smpl_id").val();
		var url = smpl.ajax + "/edit/" + id;		
		$j.ajax({
			url: url,
			type: "GET",
			dataType: "text",
			data: formData,
			success: function(data){					
				$j("div#smpl_new_tax").unbind('click');				
				$j("div#SmplEditForm").hide();
				$j("div#SmplFormTable").remove();
				d = data.trim();				
				var rows = d.split("\n");
				
				for(var i = 0 ; i< data.length ;i++){
					row = rows[i];
					r   = row.split("|");
					idx = r[0];
					if(idx == "\r") {
						e ="-1";
					}else{
						e   = r[1];
					}
					//substring change
					if(idx == "caption" && e !='-1'){
						$j("a#"+_q(id)).attr('title',e);
						$j("img#img"+_q(id)).attr('title',e);
					}
					
					if(idx == "sub" && e !='-1'){
						$j("#Sub" + _q(id)).text(e);
					}
					
					if( idx == "del" && e > 0){
						$j('td#td'+id).hide(100);	
					}
					
					if( idx == "url"){
						if(e !="-1" ){
							$j("a#LinkBtn" + id).attr("href", e);
							$j("div#link" + id).show();
						}else{
							$j("a#LinkBtn" + id).attr("href","");
							$j("div#link" + id).hide();
						}
					}
				
					if (idx == "target" && e != "-1" ){
						$j("a#linkBtn"+id).attr("target",e);
					}
				}
			},
			error :  function(result){				
				$j("div#smpl_new_tax").unbind('click');
				$j("div#SmplEditForm").hide();
				$j("div#SmplFormTable").remove();
				alert('JSON error: '+result.toString());
			},
		});
		return false;	
	});	
});